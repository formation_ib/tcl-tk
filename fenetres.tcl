# Fenetres TK
# * Example :
# Fenetres
# \- Panneau1		.panneau1
#		\- Label1	.Panneau1.label1
# \- Panneau2		.panneau2
#		\- Label2	.Panneau2.labe2

package require Tk

proc test_fenetres {} {
	tk::toplevel .fenetre2
	tk::toplevel .fenetre3
	wm title .fenetre2 "Fenetre numero 2"
	wm geometry .fenetre3 800x500+100+0
	wm attributes .fenetre3 -alpha 0.5 -topmost 1
}

wm title . "Alerte du Zoo"
wm geometry . 600x600

#Panneau 1
tk::frame .panneau1 -height 280 -relief raised -bd 2
# place .panneau1 -x 10 -y 10 -width 580 -height 280
pack .panneau1 -side top -fill x

# Dans le panneau1, de haut en bas :
# Titre
# []
# Lieu
# []
# Informations
# []


# [] Urgent
# [ Envoyer ]
# Soit 16 lignes de code :
tk::label .panneau1.titre -text {Titre}
# place .panneau1.titre -x 10 -y 10
grid .panneau1.titre -column 0 -row 0
tk::entry .panneau1.entryTitre -textvariable titre
# place .panneau1.enTitre -x 10 -y 40
grid .panneau1.entryTitre -column 1 -row 0

tk::label .panneau1.lieu -text {Lieu}
# place .panneau1.lieu -x 10 -y 70
grid .panneau1.lieu -column 0 -row 1
tk::entry .panneau1.entryLieu -textvariable lieu
# place .panneau1.entryLieu -x 10 -y 100
grid .panneau1.entryLieu -column 1 -row 1

tk::label .panneau1.informations1 -text {Informations}
# place .panneau1.informations1 -x 10 -y 130
grid .panneau1.informations1 -column 0 -row 2
tk::text .panneau1.textinformations1
# place .panneau1.textinformations1 -x 10 -y 160 -width 500 -height 60
grid .panneau1.textinformations1 -column 0 -row 3 -columnspan 2

tk::checkbutton .panneau1.checkBUrgent -text {Urgent} -variable urgent
# place .panneau1.checkBUrgent -x 10 -y 230
grid .panneau1.checkBUrgent -column 0 -row 4

tk::button .panneau1.buttonEnvoyer -text {Envoyer} -command on_buttonEnvoyer
# place .panneau1.buttonEnvoyer -x 10 -y 260
grid .panneau1.buttonEnvoyer -column 0 -row 5

#agrandir la zone de texte si possible :
grid rowconfigure .panneau1 .panneau1.textinformations1 -weight 1


#Panneau 2
tk::labelframe .panneau2 -height 280 -text Informations
# place .panneau2 -x 10 -y 310
pack .panneau2 -fill x
tk::label .panneau2.message -text {Resultat} -textvariable message
place .panneau2.message -x 10 -y 10

proc on_buttonEnvoyer {} {
	global message titre lieu urgent
	global .panneau1.textinformations1
	set infos [.panneau1.textinformations1 get 1.0 end]
	set message "Envoi de \"$titre\" a $lieu\n$infos"
	if { $urgent } {
		set message "$message\nC'est urgent !"
	}
}

# quitter avec Ctrl+q
bind . <Control-KeyPress-q> "exit"

#et pour inserer des images ou des videos ?





