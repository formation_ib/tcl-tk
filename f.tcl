# Fenetres TK
# * Exemple :
# Fenetre           .
# \- Panneau1       .panneau1 
#      \- Label1    .panneau1.label1
# \- Panneau2       .panneau2
#      \- Label2    .panneau2.label2

package require Tk

proc test_fenetres {} {
	tk::toplevel .fenetre2
	tk::toplevel .fenetre3
	wm title .fenetre2 "Fenetre numéro 2"
	# 800px de large, 500 de haut, à 100 de la gauche, à 0 du haut
	wm geometry .fenetre3 800x500+100+0
	wm attributes .fenetre3 -alpha 0.5 -topmost 1
}

wm title . "Alerte du zoo"
wm geometry . 600x600

# * Panneau 1
#tk::frame .panneau1 -width 580 -height 280 -relief sunken -bd 2
tk::frame .panneau1 -width 580 -height 280 -relief raised -bd 2
place .panneau1 -x 10 -y 10 -width 580 -height 280

# Soit 16 lignes de code :
tk::label .panneau1.lbTitre -text {Titre}
# place .panneau1.lbTitre -x 10 -y 10
grid .panneau1.lbTitre -column 0 -row 0
tk::entry .panneau1.enTitre
# place .panneau1.enTitre -x 10 -y 40
grid .panneau1.enTitre -column 1 -row 0
tk::label .panneau1.lbLieu -text {Lieu}
# place .panneau1.lbLieu -x 10 -y 70
grid .panneau1.lbLieu -column 0 -row 1
tk::entry .panneau1.enLieu
# place .panneau1.enLieu -x 10 -y 100
grid .panneau1.enLieu -column 1 -row 1
tk::label .panneau1.lbInformations -text {Informations}
# place .panneau1.lbinformations -x 10 -y 130
grid .panneau1.lbInformations -column 0 -row 2
tk::text .panneau1.teInformations
#place .panneau1.teInformations -x 10 -y 160 -width 500 -height 60
grid .panneau1.teInformations -column 0 -row 3 -columnspan 2
tk::checkbutton .panneau1.cbUrgent -text {Urgent}
# place .panneau1.cbUrgent -x 10 -y 230
grid .panneau1.cbUrgent -column 0 -row 4
tk::button .panneau1.btEnvoyer -text {Envoyer}
# place .panneau1.btEnvoyer -x 10 -y 260
grid .panneau1.btEnvoyer -column 0 -row 5
#aggrandir la zone de texte si possible :
grid rowconfigure .panneau1 .panneau1.teInformations -weight 1


# * Panneau 2
tk::labelframe .panneau2 -width 580 -height 280 -text Informations
place .panneau2 -x 10 -y 310
tk::label .panneau2.informations -text {Résultat}
# haut (et gauche) du label à 10px du haut du panneau 1
place .panneau2.informations -x 10 -y 10
#autres composants : button, entry, text (bloc), checkbutton, radiobutton,
#spinbox, listbox
