# ¤ Listes
#set bob [list lion 12 "enclos 4"]
#lappend notes fa sol
#linsert $notes 0 si
#lset animaux2 2 Aigle
#lreplace $animaux2 0 1 Chat Mouton Lion

#llength $animaux2
#lindex $animaux2 1
#foreach a $animaux2 { puts $a }

# 1 : preparer une liste de 4 lieux
# 2 : afficher toutes les trajets visitant 3 lieux differents

set lieux [ list sauvages spectacles aquatiques oiseaux ]

# while {$a > 0} {set a [expr $a-4] }
# for {set i 0} {$i < 5} {incr i} {
	# puts $i
# }

set lieux [list {Enclos des singes} Menagerie Voliere Aquarium]
for {set i 0} {$i<[llength $lieux]} {incr i} {
	for {set j 0} {$j<[llength $lieux]} {incr j} {
		for {set k 0} {$k<[llength $lieux]} {incr k} {
			if {$i!=$j && $i!=$k && $j!=$k} {
				puts "- [lindex $lieux $i] [lindex  \
					$lieux $j] [lindex $lieux $k]"
			}				
		}
	}
}

#puts "L'aquarium est le lieu n°[lsearch -nocase $lieux aquarium]"
# -ascii -integer ou -real
puts "L'aquarium est le lieu n°[lsearch -nocase -ascii $lieux aquarium]"

set lieux_ordonnes [lsort -ascii -nocase -increasing $lieux]
puts $lieux_ordonnes

puts "Lieux :"
set lieux2 "{Enclos des singes} Menagerie Voliere Aquarium Buvette"
foreach lieu $lieux2 {
	puts "- $lieu"
}

#enclos des singes
set premier_lieu [lindex $lieux2 0]

#enclos
set premier_mot [lindex $premier_lieu 0]
puts "Dans cette chaine il y a [llength $premier_mot] mot"
puts "Dans Lions,Tigres,Jaguars : [llength {Lions,Tigres,Jaguars}]"

# ¤ chaines (commande string)
puts "Dans le 1er lieu, [string length $premier_lieu] caracteres"
puts "Le 1er est : [string index $premier_lieu 0]"
puts "Nettoye : [string toupper [string trim $premier_lieu]]"
puts "Dernier d : [string last d [string totitle $premier_lieu]]"

puts " "
puts "Afficher par ordre alphabetique tous les lieux qui contiennent un e :"
foreach lieu $lieux2 {
	if {[string last e $lieu]!=-1} {
		puts "- $lieu"
	}
}


puts "\n*********Tableaux (associatifs)************\n"
set bob(espece) Lion
set bob(age) 12
set bob(lieu) {Enclos des fauves}
puts "Bob est un $bob(espece)"
#interdit : puts "Bob : $bob"
puts "Bob a [array size bob] proprietes"
#if { [lsearch [array names bob] age] !=-1} {
if { "age" in [array names bob] } {
	puts "Age de Bob : $bob(age)"
}

puts "\n************Dictionnaires**************\n"
set ann [dict create espece Giraffe age 22 lieu {Enfuie}]
dict append ann provenance Kenya
dict set ann provenance Tanzanie
puts "Ann : $ann"
puts "Ann a [dict size $ann] proprietes : [dict keys $ann]"
if { [dict exists $ann lieu] } {
	puts "Lieu de Ann : [dict get $ann lieu]"
}


set {Over the Hills and Far Away} [dict create Band "Nightwish" Album "Bestwishes" Year 2001]
dict append {Over the Hills and Far Away} Type Metal

if { [dict exists ${Over the Hills and Far Away} Band] } {
	puts [dict get ${Over the Hills and Far Away} Band]
}

puts "\nEx. A partir des lieux, afficher une correspondance : \
lettre initiale => nombre de lieu (>0)"
puts "Example : A:1, E:2, M:1, V:1"

set lieux2 "{Enclos des singes} Menagerie Voliere Aquarium Buvette {Enclos des cigognes}"
set initiales [dict create]
foreach lieu [lsort $lieux2] {
	set init [string index $lieu 0]
	if {![dict exists $initiales $init]} {
		dict append initiales $init 1
	} else {
		dict set initiales $init [expr [dict get $initiales $init]+1]
	}
}
puts $initiales

puts "\n ********** Liste de liste *********** \n"
set nbvisiteurs [list \
	[list 213 119 110 321 198] \
	[list 110 232 321 455 198] \
	[list 111 146 191 235 456]
]

# ou set nbvisiteurs { { 213 119...} {...} {...} }
puts "Visiteurs le 1er janvier : [lindex $nbvisiteurs 0 0]"
puts "Visiteurs le 3 février : [lindex $nbvisiteurs 1 2]"


puts "\n ********** Liste de dict *********** \n"
set enclos [list \
	[dict create Nom Fauves Taille 23400] \
	[dict create Nom Herbivore Taille 9100] \
	[dict create Nom Singes Taille 6700] \
]
if { [llength $enclos]>2 && [dict exists [lindex $enclos 2] Taille]} {
	puts "Superficie de l'enclos des Singes : [dict \
		get [lindex $enclos 2] Taille]"
}




puts "\n ********** Clock *********** \n"

set start [clock microseconds]
puts "Il est : [clock seconds]"
puts "Il est : [clock microseconds]"
set demain [clock add [clock seconds] 1 days]
puts "Demain : $demain"
puts "Demain : [clock format $demain -format {%Y-%m-%d %H:%M:%S}]"	
set end [clock microseconds]
puts "Temps écoulé : [expr $end-$start] µs"

% timerate { puts [expr sin(3)] } 5
# 0.1411200080598672
# 0.1411200080598672
# 0.1411200080598672
# 0.1411200080598672
# 0.1411200080598672
# 0.1411200080598672
# 0.1411200080598672
# 0.1411200080598672
# 0.1411200080598672
# 0.1411200080598672
# 515.400 µs/# 10 # 1940.2 #/sec 5.154 net-ms

time {
set lieux2 "{Enclos des singes} Menagerie Voliere Aquarium Buvette"
foreach lieu $lieux2 {
        puts "- $lieu"
}
}
# - Enclos des singes
# - Menagerie
# - Voliere
# - Aquarium
# - Buvette
# 5328 microseconds per iteration


after 5000













