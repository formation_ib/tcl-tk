#namespaces

#si je fais set dans namespace, c'est comme si cette variable etait global->niveau 0

# abc::x = x dans le namespace abc
# ::x = x dans le namespace racine
# x = x dans le meme namespace que le code en cours

#on 3 variables
namespace eval Aquarium {
	::variable longueur 27
	::variable largeur 18
	variable profondeur 6
	# 
	namespace export affiche
	# creation de commandes avec de souscommande
	namespace ensemble create
}

#premier methode
proc Aquarium::get_volume {} {
	# ::global longueur largeur profondeur
	::variable longueur
	::variable largeur
	::variable profondeur
	::return [::expr $longueur*$largeur*$profondeur]
}

#deuxieme methode
proc Aquarium::affiche {} {
	puts "Notre aquarium : [get_volume] m3 !"
}
	
	
#:: va chercehr a la recine du declaration
::puts "Informations :"
Aquarium::affiche

namespace import Aquarium::*
affiche

#creation de commandes avec de souscommande
Aquarium affiche