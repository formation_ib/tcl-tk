# threads
package require Thread

# tsv : espace pour partager lamemoire entre tous les threads
# lipins = dexieme nom que l'on donne quand on utilise le variable partager
tsv::set lapins resultat 0
tsv::set lapins2 troplong 0


# ******THREAD secondaire********
# puts "Au bout de 6 mois : [nblapins 6]"
set threadcalcul [thread::create -joinable {
	proc nblapins {mois} {
		if {$mois<3} {
			return $mois
		} elseif { [tsv::get lapins2 troplong] } {
			return Inf
		} else {
			return [expr 3*([nblapins [expr $mois-1]]+ \
					[nblapins [expr $mois-2]])/2 ]
		}
	}
	#amelioration: on verrouille la variable lapin par un "lock" qui evite que deux threads ecrit au meme temps
	tsv::lock lapins {
		tsv::set lapins resultat [nblapins 36]
	}
}]
puts "Calcul"
puts "Au bout de 2 ans et 6 mois :"


# ******THREAD principal********
# Affichage petit points
# for {set i 0} {$i<10} {incr i} {
for {set i 0} {[thread::exists $threadcalcul]} {incr i} {
	puts -nonewline "."
	# flush = decharger quelque chose -> affiche tout de suite
	flush stdout
	after 100
	if {$i == 100} {
		tsv::set lapins2 troplong 1
	}
}
tsv::lock lapins {
	puts "[tsv::get lapins resultat]"
}
# thread::join $threadcalcul



