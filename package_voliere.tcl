# declaration de package
# generation : pkg_mkIndex . package_voliere.tcl
# puis distribuer un zip avec package_voliere
package require Tcl 8.5
package provide Voliere 1.2
# package provide Voliere 1.2a3
# package provide Voliere 1.2.-2.-3

# package provide Voliere 1.2b3
# package provide Voliere 1.2.-1.-3

namespace eval Voliere {
	variable especes [list Aigles Vautours Milans Peroquets]
	namespace export afficher
	namespace export ajouter
}

proc Voliere::ajouter {animal} {
	variable especes 
	lset especes 4 $animal
}

proc Voliere::afficher {} {
	variable especes
	puts "Notre voliere accueille :"
	foreach espece $especes {
		puts "- des [string tolower $espece]"
	}
}

namespace import Voliere::*
afficher