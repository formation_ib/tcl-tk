# Classes et objets

oo::class create Animal {
	variable nom
	variable age
	variable espece
	constructor {} {}
	constructor {{n ""} {a 0} {e ""}} {
		set nom $n 
		set age $a 
		set espece $e}
	destructor { puts "$nom detruit" }
	method setNom {n} { set nom $n }
	method setAge {a} { set age $a }
	method setAgeRnMois {m} { my setAge [expr $m/12] }
	method setEspece {e} { set espece $e }
	method afficher {} { puts "$nom ($espece) a $age ans"}
}

oo::class create Encadreur {
	method encadrer {} {
		puts "****************************"
		my afficher
		puts "****************************"
	}
}

oo::class create Reptile {
	superclass Animal
	mixin Encadreur
	variable amphibien
	constructor {{n ""} {a 0} {e ""} {am ""}} {
		#next: prend les memes methodes et passe le a la class mere
		next $n $a $e
		set amphibien $am}
		method setAmphibien {am} { set amphibien $am }
		method afficher {} {
			nextto Animal
			if {$amphibien} {
				puts " - C'est un amphibien"
			} else {
				puts -nonewline " - C'est un terrien"
			}
		}
}

#sans constructeur
set ann [Animal new]
$ann setNom "Ann"
$ann setAge 23
$ann setEspece Girafe
$ann afficher

#avec constructeur
set luis [Animal new "luis" 25 Lion]
$luis afficher

#destructeur
$luis destroy

#constructuer fille
set frank [Reptile new "Phil" 3 Caiman 1]
$frank afficher
puts ""
$frank encadrer

