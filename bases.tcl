# exécution : > tclsh -encoding utf-8 base.tcl

puts "Bienvenue au Zoo"

# la place est a 8.5e, mais a partir de 5 personnes,
# c'est a 6.30/personne au dela de 4.
# Afficher le prix pour 1 et pour 6

# set prix_un 8.5
# set prix_6 6.30
# set porcetage [expr $prix_6*100/$prix_un]

# puts "Une personne : $prix_un £, 6 personnes : $prix_6 £"

set place14 8.5
set place5plus 6.3

set prix_un $place14
set prix_6 [expr $place14*4+$place5plus*2]


puts "Une personne : $prix_un £, 6 personnes : $prix_6 £"

#**************************************************************
# afficher le tarif en fonction du nombre de visiteurs,
# en offrant du popcorn (1 paquet pour 3 visiteurs, si au moins 
# 4 visiteurs)

# Example
# % if {$a < 100} {
        # puts 1
# } elseif { $a < 200 } {
# % } elseif { $a < 200 } {
        # puts 2
# } else {
        # puts 3
# }

puts "Nombre de visiteurs"
gets stdin visiteurs

if {$visiteurs<4} {
	set prix [expr $visiteurs*$place14]
	set popcorns 0
} else {
	if {$visiteurs<5} {
		set prix [expr  $visiteurs*$place14]
	} else {
		set prix [expr  4*$place14+($visiteurs-4)*$place5plus]
	}
	set popcorns [expr $visiteurs/3]
}

puts "Prix total : $prix, offert : $popcorns popcorns"

switch $visiteurs {
	1 { puts "Bienvenue, visiteur" }
	2 -
	3 -
	4 {puts "Bienvenue, famille de visiteurs" }
	default { puts "Bienvenue, groupe de visiteurs" }
}
	
# while {$a > 0} {set a [expr $a-4] }
# for {set i 0} {$i < 5} {incr i} {
	# puts $i
# }

# Afficher :
# 1 jour : ...euros
# 2 jours : ...euros
# ... 7 jours

for {set j 1} {$j<=7} {incr j} {
	puts "$j jour(s) : [expr $prix*$j] euros"
}



after 5000





























