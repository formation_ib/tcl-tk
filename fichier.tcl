# fichiers

#commande Catch reenvoit en vrai ou faux

# if {[catch { code??dangereux?? } err]} {
	# puts "Error : $err"
# }

if {[catch { [expr 3/0] } err]} {
	puts "Erreur : $err"
}

#
if {[catch { error "Plus de cacahuettes" } err]} {
	puts "Erreur : $err"
}

#Code de erreurs
if {[catch { error "Plus de cacahuettes" "Enclos des singes" 10 } err]} {
	puts "Erreur : $err ! $ errorInfo ($errorCode)"
}

#voir le repertoire en cours
if {[catch {
	puts "Repertoire en cours : [pwd]"
} err]} {
	puts "Erreur : $err ! $errorInfo ($errorCode)"
}

#creer un repertoire en cours s'il n'existe pas
# if {[catch {
	# if {![file exists data]} {
		# file mkdir data
	# }
	# cd data
	# puts "Repertoire en cours : [pwd]"
# } err]} {
	# puts "Erreur : $err ! $errorInfo ($errorCode)"
# }

#creer un fichier dans le repertoire data
if {[catch {
	if {![file exists data]} {
		file mkdir data
	}
	cd data
	puts "Repertoire en cours : [pwd]"
	foreach fichier [glob -nocomplain *.*] {
		puts "- $fichier"
	}
	# log210408.txt
	set nomfichier "log[clock format [clock seconds] -format %y%m%d].txt"
	puts "Creation de $nomfichier"
	set f1 [open $nomfichier a]
	# 2021-04-08 15:48:56 ok
	puts $f1 "[clock format [clock seconds] -format {%Y-%m-%d %H:%M:%S}] ok"
	close $f1
	
	set f2 [open $nomfichier r]
	set nblignes 0
	while { [gets $f2 ligne] >= 0 } {
		incr nblignes
	}
	close $f2
	puts "$nblignes lignes de log dans le fichier d'aujourd'hui"
} err]} {
	puts "Erreur : $err ! $errorInfo ($errorCode)"
}




















